== Resultats d’aprenentatge i criteris d’avaluació:



....... **Administra** serveis de resolució de noms, analitzant-los i garantint la seguretat del servei.



.... **Identifica** i descriu escenaris en els quals sorgeix la necessitat d'un servei de resolució de noms.



.... **Classifica** els principals mecanismes de resolució de noms.



.... **Descriu** l'estructura, la nomenclatura i la funcionalitat dels sistemes de noms jeràrquics.



.... **Instal·la** i configura serveis jeràrquics de resolució de noms.



.... **Prepara** el servei per reexpedir consultes de recursos externs a un altre servidor de noms



.... **Prepara** el servei per emmagatzemar i distribuir les respostes procedents d'altres servidors.



.... **Afegeix** registres de noms corresponents a una zona nova, amb opcions relatives a servidors de correu i àlies.



.... **Implementa** solucions de servidors de noms en adreces IP dinàmiques.



.... **Realitza** transferències de zona entre dos o més servidors.



.... **Documenta** els procediments d’instal·lació i configuració.







....... **Administra** serveis de configuració automàtica, identificant-los i verificant la correcta assignació dels paràmetres.



.... **Reconeix** els mecanismes automatitzats de configuració dels paràmetres de xarxa i els avantatges que proporcionen.



.... **Il·lustra** els procediments i les pautes que intervenen en una sol•licitud de configuració dels paràmetres de xarxa.



.... **Instal·la** servidors de configuració dels paràmetres de xarxa.



.... **Prepara** el servei per assignar la configuració bàsica als equips d'una xarxa local.



.... **Configura** assignacions estàtiques i dinàmiques.



.... **Integra** en el servei opcions addicionals de configuració.



.... **Documenta** els procediments realitzats.

